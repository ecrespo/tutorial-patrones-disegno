#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Prototipo
Crea nuevos objetos duplicandolos, clonando una instancia creada previamente.

Este patrón especifica la clase de objetos a crear mediante la clonación de un
prototipo que es una instancia ya creada. La clase de los objetos que servirán
de prototipo deberá incluir en su interfaz la manera de solicitar una copia,
que será desarrollada luego por las clases concretas de prototipos.

Este patrón nos resulta útil en escenarios donde es impreciso abstraer la lógica
 que decide qué tipos de objetos utilizará una aplicación, de la lógica que
 luego usarán esos objetos en su ejecución. Los motivos de esta separación
 pueden ser variados, por ejemplo, puede ser que la aplicación deba basarse en
 alguna configuración o parámetro en tiempo de ejecución para decidir el tipo
 de objetos que se debe crear. En ese caso, la aplicación necesitará crear
 nuevos objetos a partir de modelos. Estos modelos, o prototipos, son clonados
 y el nuevo objeto será una copia exacta de los mismos, con el mismo estado.
 Como decimos, esto resulta interesante para crear, en tiempo de ejecución,
 copias de objetos concretos inicialmente fijados, o también cuando sólo existe
  un número pequeño de combinaciones diferentes de estado para las instancias
  de una clase.

Dicho de otro modo, este patrón propone la creación de distintas variantes de
objetos que nuestra aplicación necesite, en el momento y contexto adecuado.
Toda la lógica necesaria para la decisión sobre el tipo de objetos que usará
la aplicación en su ejecución se hace independiente, de manera que el código
que utiliza estos objetos solicitará una copia del objeto que necesite. En
este contexto, una copia significa otra instancia del objeto. El único requisito
 que debe cumplir este objeto es suministrar la funcionalidad de clonarse.

En el caso, por ejemplo, de un editor gráfico, podemos crear rectángulos,
círculos, etc... como copias de prototipos. Estos objetos gráficos pertenecerán
a una jerarquía cuyas clases derivadas implementarán el mecanismo de clonación.
https://es.wikipedia.org/wiki/Prototype_(patr%C3%B3n_de_dise%C3%B1o)

https://code.activestate.com/recipes/86651-prototype-pattern/
https://gist.github.com/pazdera/1122366
"""

from copy import deepcopy

class Prototipo:
    def __init__(self):
        self._objetos = {}

    def registrarObjetos(self, nombre, objeto):
        """
        register an object.
        """
        self._objetos[nombre] = objeto

    def borrarRegistroObjecto(self, nombre):
        """unregister an object"""
        del self._objetos[nombre]

    def clonar(self, nombre, **attr):
        """clone a registered object and add/replace attr"""
        objeto = deepcopy(self._objetos[nombre])
        objeto.__dict__.update(attr)
        return objeto


if __name__ == '__main__':
    class A:
        pass
    a=A()
    prototipo=Prototipo()
    prototipo.registrarObjetos("a",a)
    b=prototipo.clonar("a",a=1,b=2,c=3)
    print(b.a)
    print(b.b)
    print (b.c)
