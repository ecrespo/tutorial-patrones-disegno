#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de Estructural
Adapter
"""

class FoobarWrapper(object):
    def __init__(self,wrappee):
        self.w = wrappee

    def foobar(self,foo,bar):
        return self.w.barfoo(bar,foo)

class Foobarer(Barfooer):
    def foobar(self, foo,bar):
        return self.barfoo(bar,foo)
        

foobarer = FoobarWrapper(barfooer)
