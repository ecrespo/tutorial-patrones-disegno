#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Singleton
1. permite la creación de sólo una instancia de la clase singleton.
2. si una instancia existe se le sirve el mismo objeto de nuevo.
"""

class Singleton(object):
    def __new__(cls):
        if not hasattr(cls,'instance'):
            cls.instance = super(Singleton,cls).__new__(cls)
        return cls.instance

if __name__=="__main__":
    s1 = Singleton()
    print ('El objeto ha sido creado-> ',s1)
    s2 = Singleton()
    print ('El objeto ha sido creado-> ',s2)
