#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Singleton
1. permite la creación de sólo una instancia de la clase singleton.
2. si una instancia existe se le sirve el mismo objeto de nuevo.
Con metaclases
"""

import sqlite3
class MetaSingleton(type):
    _instances = {}
    def __call__(cls,*args,**kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton,cls).__call__(*args,**kwargs)
        return cls._instances[cls]

class Database(metaclass=MetaSingleton):
    connection = None
    def connect(self):
        if self.connection == None:
            self.connection= sqlite3.connect('db.sqlite3')
            self.cursorobj = self.connection.cursor()
        return self.cursorobj

if __name__ == '__main__':
    db1 = Database().connect()
    db2 = Database().connect()
    print (db1)
    print(db2 )
