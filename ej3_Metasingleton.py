#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Singleton
1. permite la creación de sólo una instancia de la clase singleton.
2. si una instancia existe se le sirve el mismo objeto de nuevo.
Con metaclases
"""

class MetaSingleton(type):
    _instances = {}
    def __call__(cls,*args,**kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton,cls).__call__(*args,**kwargs)
        return cls._instances[cls]

class Logger(metaclass=MetaSingleton):
    pass

if __name__=="__main__":
    logger1= Logger()
    logger2 = Logger()
    print(logger1,logger2)
