#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Singleton
1. permite la creación de sólo una instancia de la clase singleton.
2. si una instancia existe se le sirve el mismo objeto de nuevo.
"""

class Singleton:
    __instance = None
    def __init__(self):
        if not Singleton.__instance:
            print ('metodo __init__ llamado')
        else:
            print ('Instancia ya existe :', self.getInstance())

    @classmethod
    def getInstance(cls):
        if not cls.__instance:
            cls.__instance = Singleton()
        return cls.__instance


if __name__=="__main__":
    s1 = Singleton()
    print ('El objeto ha sido creado-> ',Singleton().getInstance())
    print ('El objeto ha sido creado-> ',s1.getInstance())
    s2 = Singleton()
    print ('El objeto ha sido creado-> ',s2.getInstance())
