#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o
Patrón Estructural
Observador
Define una dependencia de uno-a-muchos entre objetos, de forma que cuando un
objeto cambie de estado se notifique y actualicen automáticamente todos los
objetos que dependen de él.
Si se necesita consistencia entre clases relacionadas, pero con independencia,
es decir, con un bajo acoplamiento.
Puede pensarse en aplicar este patrón cuando una modificación en el estado de un
 objeto requiere cambios de otros, y no deseamos que se conozca el número de
 objetos que deben ser cambiados. También cuando queremos que un objeto sea
 capaz de notificar a otros objetos sin hacer ninguna suposición acerca de los
 objetos notificados y cuando una abstracción tiene dos aspectos diferentes, que
 dependen uno del otro; si encapsulamos estos aspectos en objetos separados
 permitiremos su variación y reutilización de modo independiente.

https://es.wikipedia.org/wiki/Observer_(patr%C3%B3n_de_dise%C3%B1o)

"""
class Sujeto:
    def __init__(self):
        self.__observadores= []

    def registrar(self,observador):
        self.__observadores.append(observador)

    def notificarTodos(self,*args,**kwargs):
        for observador in self.__observadores:
            observador.notificar(self,*args,**kwargs)

class Observador1:
    def __init__(self,sujeto):
        sujeto.registrar(self)

    def notificar(self,sujeto,*args):
        print(type(self).__name__,'::obtener',args,'desde ',sujeto)

class Observador2:
    def __init__(self,sujeto):
        sujeto.registrar(self)

    def notificar(self,sujeto,*args):
        print(type(self).__name__,'::obtener',args,'desde ',sujeto)

if __name__ == '__main__':
    sujeto = Sujeto()
    observador1 = Observador1(sujeto)
    observador2 = Observador2(sujeto)
    sujeto.notificarTodos('notificacion')
