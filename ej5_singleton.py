#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Singleton
1. permite la creación de sólo una instancia de la clase singleton.
2. si una instancia existe se le sirve el mismo objeto de nuevo.
Con metaclases
"""


class HealthCheck:
    _instance = None
    def __new__(cls,*args,**kwargs):
        if not HealthCheck._instance:
            HealthCheck._instance = super(HealthCheck,cls).__new__(cls,*args,**kwargs)
        return HealthCheck._instance

    def __init__(self):
        self._servers = []

    def addServer(self):
        self._servers.append("Servidor 1")
        self._servers.append("Servidor 2")
        self._servers.append("Servidor 3")
        self._servers.append("Servidor 4")

    def changeServer(self):
        self._servers.pop()
        self._servers.append('Servidor 5')

if __name__ == '__main__':
    hc1 = HealthCheck()
    hc2 = HealthCheck()
    hc1.addServer()
    print("Agendado check servidor (1)...")
    for i in range(4):
        print ("chequeando...", hc1._servers[i])

    hc2.changeServer()
    print("Agendando check servidor (2)...")
    for i in range(4):
        print ("chequeando...", hc2._servers[i])
