#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón Estructural
https://es.wikipedia.org/wiki/Proxy_(patr%C3%B3n_de_dise%C3%B1o)
Proxy
"""

class Cliente:
    def __init__(self):
        print("Tu: vas a comprar una camisa")
        self.tarjetaDebito = TarjetaDebito()
        self.estaComprando = None

    def hacerPago(self):
        self.estaComprando = self.tarjetaDebito.hacer_pago()

    def __del__(self):
        if self.estaComprando:
            print (u"Tu: La camisa es mía")
        else:
            print(u"Tu: debo reunir más dinero")

from abc import ABCMeta, abstractmethod
class Pago(metaclass=ABCMeta):
    @abstractmethod
    def hacer_pago(self):
        pass

class Banco(Pago):
    def __init__(self):
        self.tarjeta = None
        self.cuenta = None

    def __obtenerCuenta(self):
        self.cuenta = self.tarjeta
        return self.cuenta

    def __tieneFondos(self):
        print ("Banco: verificando si la cuenta",self.__obtenerCuenta(), " tiene suficientes fondos")
        return True

    def asignarTarjeta(self,tarjeta):
        self.tarjeta = tarjeta

    def hacer_pago(self):
        if self.__tieneFondos():
            print ("Banco: Pagando la mercancia")
            return True
        else:
            print ("Banco: Lo siento, no tiene suficiente fondos!")
            return False

class TarjetaDebito(Pago):
    def __init__(self):
        self.banco = Banco()

    def hacer_pago(self):
        tarjeta = input(u"Proxy:: escriba su número de tarjeta: ")
        self.banco.asignarTarjeta(tarjeta)
        return self.banco.hacer_pago()


if __name__ == '__main__':
    cliente = Cliente()
    cliente.hacerPago()
