#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón Estructural
Fachada
Provee de una interfaz unificada simple para acceder a una interfaz o grupo de
interfaces de un subsistema.
Los patrones de diseño dan una solución probada y documentada a problemas de
desarrollo de software que aparecen en un contexto similar. El patrón de diseño
Fachada (Facade) es un tipo de patrón estructural.

El patrón fachada viene motivado por la necesidad de estructurar un entorno de
programación y reducir su complejidad con la división en subsistemas,
minimizando las comunicaciones y dependencias entre estos.

Se aplicará el patrón fachada cuando se necesite proporcionar una interfaz
simple para un subsistema complejo, o cuando se quiera estructurar varios
subsistemas en capas, ya que las fachadas serían el punto de entrada a cada
nivel. Otro escenario proclive para su aplicación surge de la necesidad de
desacoplar un sistema de sus clientes y de otros subsistemas, haciéndolo más
independiente, portable y reutilizable (esto es, reduciendo dependencias entre
los subsistemas y los clientes).
https://es.wikipedia.org/wiki/Facade_(patr%C3%B3n_de_dise%C3%B1o)
"""
class ManejadorEvento(object):
    def __init__(self):
        print ("Manejador de eventos: Dejame hablar con los socios")

    def arreglo(self):
        self.hotel =	Hotel()
        self.hotel.reservando()
        self.floristeria = Floristeria()
        self.floristeria.asignarRequerimientosFlores()
        self.catering = Catering()
        self.catering.asignarCocina()
        self.musica =	Musica()
        self.musica.asignarTipoMusica()

class Hotel(object):
    def __init__(self):
        print("Hotel: Reservando el hotel para el matrimonio")

    def __estaDisponible(self):
        print(u"Hotel: El hotel está disponible para el día de la boda")
        return True

    def reservando(self):
        if self.__estaDisponible():
            print ("Hotel: Reservado el hotel")

class Floristeria(object):
    def __init__(self):
        print(u"Floristeria: Flores para la decoración de la boda?")

    def asignarRequerimientosFlores(self):
        print(u"Floristeria: Rosas y lilas serán usadas para la decoración? ---")

class Catering(object):
    def __init__(self):
        print(u"Catering: Comida para el evento--")

    def asignarCocina(self):
        print(u"Catering: Cocina china y continental será servida\n")

class Musica(object):
    def __init__(self):
        print(u"Musicos: Arreglo músical para la boda")

    def asignarTipoMusica(self):
        print(u"Muicos: Jazz y música clásica será tocada durante la boda")


class Cliente(object):
    def __init__(self):
        print("Cliente: Guao! arreglo de la boda!!")

    def preguntarManejadorEvento(self):
        print ("Cliente: Dejame contactar al manejador de eventos")
        em = ManejadorEvento()
        em.arreglo()

    def __del__(self):
        print(u"Cliente: Gracias al manejador de eventos, toda la preparación de la boda está lista")

if __name__ == "__main__":
    cliente = Cliente()
    cliente.preguntarManejadorEvento()
