#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Metodo factory

https://es.wikipedia.org/wiki/Factory_Method_(patr%C3%B3n_de_dise%C3%B1o)
"""

from abc import ABCMeta, abstractmethod

class Section(metaclass=ABCMeta):
    @abstractmethod
    def describe(self):
        pass

class PersonalSection(Section):
    def describe(self):
        print ("Seccion personal")

class AlbumSection(Section):
    def describe(self):
        print (u"sección Album")

class PatentSection(Section):
    def describe(self):
        print(u"Sección patent")

class PublicationSection(Section):
    def describe(self):
        print(u'Sección publicación')


class Profile(metaclass=ABCMeta):
    def __init__(self):
        self.sections = []
        self.createProfile()

    @abstractmethod
    def createProfile(self):
        pass

    def getSections(self):
        return self.sections

    def addSection(self,section):
        self.sections.append(section)


class Linkedin(Profile):
    def createProfile(self):
        self.addSection(PersonalSection())
        self.addSection(PatentSection())
        self.addSection(PublicationSection())

class Facebook(Profile):
    def createProfile(self):
        self.addSection(PersonalSection())
        self.addSection(AlbumSection())

if __name__ == '__main__':
    profile_type = input(u"Cual perfil le gustaría crear Facebook o Linkedin? ")
    profile = eval(profile_type)()
    print("Creando el perfil ", type(profile).__name__)
    print("Las secciones del perfil son--> ", profile.getSections())
