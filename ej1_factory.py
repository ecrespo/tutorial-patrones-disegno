#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Simple factory
Ayuda a crear objetos de diferentes tipos en lugar de instanciación de objetos directos.
Se crea un producto abstracto llamado Animal.
Animal es una clase base abstracta.
Se crean dos productos, gato y perro, se implementa el método dice, con el sonido apropiado que cada animal hace.
La fabrica bosque, es una fábrica que hace el método hacer_sonido().
Basado en el tipo de argumento que se le pasa al cliente, se instancia un animal, y crea en tiempo de ejecución, el sonido
correspondiente a la salida.
"""

from abc import ABCMeta,abstractmethod

class Animal(metaclass=ABCMeta):
    @abstractmethod
    def dice(self):
        pass

class Perro(Animal):
    def dice(self):
        print ("guau guau")

class Gato(Animal):
    def dice(self):
        print ('miau miau')

#factory bosque
class BosqueFactory(object):
    def haceSonido(self,object_type):
        return eval(object_type)().dice()

#Codigo del cliente
if __name__ == '__main__':
    ff = BosqueFactory()
    animal = input("Que sonido hace un Perro o un Gato? >")
    ff.haceSonido(animal)
