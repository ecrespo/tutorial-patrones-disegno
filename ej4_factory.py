#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
Contexto: Debemos crear diferentes objetos, todos pertenecientes a la misma
familia. Por ejemplo: las bibliotecas para crear interfaces gráficas suelen
utilizar este patrón y cada familia sería un sistema operativo distinto. Así
pues, el usuario declara un Botón, pero de forma más interna lo que está
creando es un BotónWindows o un BotónLinux, por ejemplo.

El problema que intenta solucionar este patrón es el de crear diferentes
familias de objetos.

El patrón Abstract Factory está aconsejado cuando se prevé la inclusión de
nuevas familias de productos, pero puede resultar contraproducente cuando se
añaden nuevos productos o cambian los existentes, puesto que afectaría a todas
las familias creadas.
https://es.wikipedia.org/wiki/Abstract_Factory
"""
from abc import ABCMeta, abstractmethod

class PizzaFactory(metaclass=ABCMeta):
    @abstractmethod
    def createVegPizza(self):
        pass

    @abstractmethod
    def createNonVegPizza(self):
        pass


class IndianPizzaFactory(PizzaFactory):
    def createVegPizza(self):
        return DeluxVeggiePizza()

    def createNonVegPizza(self):
        return ChickenPizza()

class USPizzaFactory(PizzaFactory):
    def createVegPizza(self):
        return MexicanVegPizza()

    def createNonVegPizza(self):
        return HamPizza()

class VegPizza(metaclass=ABCMeta):
    @abstractmethod
    def prepare(self,VegPizza):
        pass

class NonVegPizza(metaclass=ABCMeta):
    @abstractmethod
    def serve(self,VegPizza):
        pass

class DeluxVeggiePizza(VegPizza):
    def prepare(self):
        print ("Prepare",type(self).__name__)

class ChickenPizza(NonVegPizza):
    def serve(self,VegPizza):
        print(type(self).__name__,	"	is	served	with	Chicken	on	",
        type(VegPizza).__name__)

class MexicanVegPizza(VegPizza):
    def prepare(self):
        print("Prepare",type(self).__name__)

class HamPizza(NonVegPizza):
    def serve(self,VegPizza):
        print(type(self).__name__,	"	is	served	with	Ham	on	",
        type(VegPizza).__name__)

class PizzaStore:
    def __init__(self):
        pass

    def makePizzas(self):
        for	factory	in	[IndianPizzaFactory(),	USPizzaFactory()]:
            self.factory = factory
            self.NonVegPizza = self.factory.createNonVegPizza()
            self.VegPizza = self.factory.createVegPizza()
            self.VegPizza.prepare()
            self.NonVegPizza.serve(self.VegPizza)


if __name__ == '__main__':
    pizza = PizzaStore()
    pizza.makePizzas()
