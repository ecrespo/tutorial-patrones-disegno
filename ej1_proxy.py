#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón Estructural
Proxy
El patrón Proxy es un patrón estructural que tiene como propósito proporcionar
un subrogado o intermediario de un objeto para controlar su acceso.

https://es.wikipedia.org/wiki/Proxy_(patr%C3%B3n_de_dise%C3%B1o)

"""


class Actor(object):
    def __init__(self):
        self.estaOcupado = False

    def ocupado(self):
        self.estaOcupado = True
        print(type(self).__name__, u"Está ocupado actualmente con una película")

    def disponible(self):
        self.estaOcupado = False
        print(type(self).__name__,u"está disponible para hacer una película")

    def obtenerEstatus(self):
        return self.estaOcupado


class Agente(object):
    def __init__(self):
        self.principal = None

    def trabajo(self):
        self.actor = Actor()
        if self.actor.obtenerEstatus():
            self.actor.ocupado()
        else:
            self.actor.disponible()

if __name__ == "__main__":
    r = Agente()
    r.trabajo()
    
