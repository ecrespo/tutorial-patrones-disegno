#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""metaclase"""

class MiEntero(type):
    def __call__(cls,*args,**kwargs):
        print ("Aqui mi entero", args)
        print("Ahora a hacer lo que quiera al objeto")
        return type.__call__(cls,*args,**kwargs)


class int(metaclass=MiEntero):
    def __init__(self,x,y):
        self.x = x
        self.y = y


if __name__ == '__main__':
    i = int(4,5)
