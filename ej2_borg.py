#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Patrón de creación
monoestado - borg
Es un caso particular de Singleton.
Varios objetos comparten el mismo estado.
"""

class Borg(object):
    __share_state = {}
    def __new__(cls,*args,**kwargs):
        obj = super(Borg,cls).__new__(cls, *args,**kwargs)
        obj.__dict__ = cls.__share_state
        return obj
        
if __name__ == '__main__':
    b = Borg()
    b1 = Borg()
    b.x = 4
    print('El objeto borg b: ', b)
    print('El objeto borg b1:', b1)
    print('El estado del objeto b:', b.__dict__)
    print('El estado del objeto b1: ',b1.__dict__)
